package com.epam.learn.l21;

public class BinarySearchEx {
    public static void main(String[] args) {
        int[] array = new int[]{1,3,25,254,244};
        new BinarySearchEx().binarySearch(array, 0, array.length -1, 25);
    }

    void binarySearch(int[] array, int first, int last, int item) {
        int position;
        int comparsionCount = 1;

        position = (first + last) / 2;

        while ((array[position]) != item && (first <= last)) {
            comparsionCount++;

            if(array[position] > item) {
                last = position - 1;
            } else {
                first = position + 1;
            }
            position = (first + last) / 2;
        }

        if (first <= last) {
            System.out.println(item + " is " +  ++position + " in array");
            System.out.println("We found an item in " + comparsionCount);
        } else {
            System.out.println("Not found");
        }

    }
}

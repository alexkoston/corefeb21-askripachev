package com.epam.learn.l21;

public class Main {
    private static int[] array = new int[]{1,3,25,254,244};
    public static void main(String[] args) {
        // O
        // O(1) - константная сложность
        // O(logN) - логарифмическая сложность
        // O(N) - линейная сложность
        // O(N * logN) - логарифмическая сложность
        // O(N * N) - линейная сложность

        int i = 134;
        boolean isInArray = false;
        for (int j = 0; j < array.length ; j++) {
            if(array[j] == i)
                isInArray = true;
        }
        System.out.println("The number " + i + " in array" + isInArray);
    }
}

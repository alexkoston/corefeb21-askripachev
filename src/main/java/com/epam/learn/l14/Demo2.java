package com.epam.learn.l14;

public class Demo2 {
    public static void main(String[] args) {
        Thread thread = Thread.currentThread();
        System.out.println("Current thread name: " + thread.getName());
        thread.setName("My thread");
        System.out.println("After changing name: " + thread.getName());

        try {
            for (int i = 5; i > 0 ; i--) {
                System.out.println(i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

package com.epam.learn.l9;

public class  Main3 <T>{
    T type;

    public Main3(T type) {
        this.type = type;
    }

    public static void main(String[] args) {
        Main3 <Integer>  main3 = new Main3<>(2);
        Main3<Double> main31 = new Main3<>(123.0);
        Main3 main32 = new Main3(1);

        main3.getSmth(main3);
//        main3.getSmth(main31);

        main3.getSmth1(main3);
        main3.getSmth1(main31);

        main3.getSmth2(main3);
        main3.getSmth2(main31);
    }
    public  T getSmth(Main3<T> t){
        System.out.println(t.getClass().getName());
        return t.type;
    }
    public  void getSmth1(Main3<?> t){
        System.out.println(t.getClass().getName());
    }
    public  void getSmth2(Main3 t){
        System.out.println(t.getClass().getName());
    }
}

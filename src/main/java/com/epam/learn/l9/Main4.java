package com.epam.learn.l9;

public class Main4 {
    public static void main(String[] args) {
        new Main4().getFood("Barsik");
        new Main4().getFood("Murzik");
        new Main4().getFood("Snejok");

    }

    void getFood(String name) {
        if(name.equals("Barsik")) {
            System.out.println("Eating...");
        } else if (name.equals("Murzik")) {
            System.out.println("Murzik waiting 8 p.m.");
        } else {
            throw new IllegalArgumentException("Access to foof restricted to:" + name);
        }
        System.out.println("Food granted to: " + name);
    }
}

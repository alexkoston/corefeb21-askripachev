package com.epam.learn.l18;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Ex2 {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(10,20,30);
//        list.stream().map(o -> o+ 1).mapToLong(o -> o).forEach(System.out::println);
//        list.stream().map(o -> o+ 1).mapToDouble(o -> o).forEach(System.out::println);
//        list.stream().map(o -> o+ 1).mapToInt(o -> o).forEach(System.out::println);

//        IntStream.of(1).mapToObj(o -> o + " Cats").forEach(System.out::println);

//        list.stream().flatMapToInt(o -> IntStream.range(0, o)).forEach(System.out::println);

//        Stream.of(120,130,140,150,220,230).limit(3).forEach(System.out::println);
//        Stream.of(120,130,140,150,220,230).skip(3).forEach(System.out::println);
//        Stream.of(8,15,4,7,11,2).sorted().forEach(System.out::println);
//        Stream.of(8,15,4,7,11,2,2,15,4,8).distinct().forEach(System.out::println);
//        Stream.of(8,15,8,8,4,8,8,8,15)
//                .peek(o -> System.out.format("before distinct : %d%n",o))
//                .distinct()
//                .peek(o -> System.out.format("after distinct : %d%n",o))
//                .map(o -> o * o)
//                .forEach(o -> System.out.format("after map : %d%n", o));

//        Stream.of(8,15,8,8,4,8,8,8,15)
//                .takeWhile(o -> o > 5).forEach(System.out::println);
//        IntStream.range(0, 100_000)
//                .parallel()
//                .filter(o -> o % 10000 == 0)
//                .map(o -> o / 10000)
//                .forEach(System.out::print);
//        System.out.println();
//        IntStream.range(0, 100_000)
//                .parallel()
//                .filter(o -> o % 10000 == 0)
//                .map(o -> o / 10000)
//                .forEachOrdered(System.out::print);

//        System.out.println(Stream.of(8,15,6,4,4,7,3,7,4).count());
//
//        List<Integer> list1 = Stream.of(1,2,3).collect(Collectors.toList());
//        System.out.println(list1);
//
//        String value = Stream.of(1,2,3).map(String::valueOf).collect(Collectors.joining(":", "<", ">"));
//        System.out.println(value);

//        String[] elements = Stream.of("barsik", "mursik", "snegok").toArray(String[]::new);
//        System.out.println(Arrays.toString(elements));

//        List<String> elements = Stream.of("barsik", "mursik", "snegok")
//                .map(String::toUpperCase).collect(Collectors.toList());

//        System.out.println(IntStream.range(2, 30_000)
//                .parallel()
//                .findFirst().getAsInt());

//        int min = Stream.of(15,22,4,5,11).min(Integer::compare).get();
//        System.out.println(min);
//        int max = Stream.of(15,22,4,5,11).max(Integer::compare).get();
//        System.out.println(max);
//
//        System.out.println(Stream.of(1,2,3).allMatch(o -> o ==2));
//        System.out.println(Stream.of(1,2,3).anyMatch(o -> o ==2));
//        System.out.println(Stream.of(1,2,3).noneMatch(o -> o ==2));

//        System.out.println(IntStream.range(1, 8)
//                .average().getAsDouble());
//
//        System.out.println(IntStream.range(1, 8)
//                .sum());

//        Map<String, String> map = new HashMap<>();
//        map.entrySet().stream().max(Map.Entry.comparingByValue());




    }
}

package com.epam.learn.l18;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Stream.empty(); // пустой стрим строк
        List<String> list = new ArrayList<>();
        list.stream();
        Map<String, String > map = new HashMap<>();
        map.entrySet().stream(); // Strean<Map.Entry<String,String>>
        Arrays.stream(args);
        Stream.of("Barsik", "Mursik", "Snegok");

        String[] arrays = new String[]{"a","barsik", "snegok"};
        List<String> list1 = Arrays.stream(arrays).filter(s -> s.length() > 1).collect(Collectors.toList());
        System.out.println(list1);

    }
}

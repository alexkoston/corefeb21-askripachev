package com.epam.learn.l11;

import java.util.HashSet;
import java.util.Set;

public class Ex {
    public static void main(String[] args) {
        Set<Cat> set = new HashSet<>();
        Cat barsik = new Cat("barsik");
        Cat mursik = new Cat("mursik");

        set.add(barsik);
        set.add(mursik);

        System.out.println(set);

        Cat barsik2 = new Cat("barsik");
        set.add(barsik2);
        System.out.println(barsik.equals(barsik2));
        System.out.println(barsik.hashCode());
        System.out.println(barsik2.hashCode());
        System.out.println(set);


    }
}

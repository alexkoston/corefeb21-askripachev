package com.epam.learn.l11;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Cat barsik = new Cat("barsik", 2);
        Cat mursik = new Cat("mursik", 1);

        System.out.println(barsik.hashCode());
        System.out.println(mursik.hashCode());

        Set<Cat> set = new HashSet<>();
        set.add(barsik);
        set.add(mursik);

        System.out.println(set);

        //Array / Collection / Map
        Collection collection;
        Map map;

        // Legacy
        Vector vector;
        Stack stack;
        Hashtable hashtable;
        Properties properties;
        BitSet bitSet;
        Enumeration enumeration;

    }
}

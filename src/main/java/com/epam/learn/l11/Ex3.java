package com.epam.learn.l11;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Ex3 {
    public static void main(String[] args) {
        Set<Cat> set = new HashSet<>();
        Cat barsik = new Cat("barsik");
        Cat mursik = new Cat("mursik");
        set.add(barsik);
        set.add(mursik);
        System.out.println(set);
        System.out.println(set.contains(barsik));

        System.out.println(barsik.hashCode());
        // field based on hashcode
        barsik.setName("Barsik");
        System.out.println(barsik.hashCode());
        System.out.println(set);
        System.out.println(set.contains(barsik));
        set.add(barsik);
        System.out.println(set);

        Set<Cat> set2 = new HashSet<>();
        set2.add(new Cat("afs"));
        set2.addAll(set);

        System.out.println(set2);

        Set<Cat> set3 = new TreeSet<>();

    }
}

package com.epam.learn.l4;

public class Main {
    public static void main(String[] args) {
        int i = 0;
        // break - прерывание циклв
        // continue - скипаем продолжение и запускаем следующиую итерацию цикла
        while (true) {
            i++;
            if (i == 50) {
                continue;
            }
            System.out.println(i);

            if(i ==100){
                break;
            }
        }
    }
}

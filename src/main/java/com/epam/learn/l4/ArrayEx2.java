package com.epam.learn.l4;

import java.util.Arrays;

public class ArrayEx2 {
    public static void main(String[] args) {
        Cat barsik = new Cat("barsik", 2);
        Cat murzik = new Cat("murzik", 1);

        Cat[] catsArr = new Cat[2];
        catsArr[0] = barsik;
        catsArr[1] = murzik;

        System.out.println(Arrays.toString(catsArr));

    }
}

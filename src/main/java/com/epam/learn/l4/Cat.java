package com.epam.learn.l4;

public class Cat {
    private String name;
    private int age;

    public Cat(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String toString(){
        return name;
    }
}

package com.epam.learn.l12;

import java.util.*;
import java.util.concurrent.*;

public class Main2 {
    // General - purpose : HashSet, TreeSet, LinkedHashSet, ArrayList, LinkedList, BlockingDeque, PriorityQueue
    // Special - purpose : EnumSet, EnumMap, CopyOnWriteArraySet, CopyOnWriteArrayList
    // Concurrent : ConcurrentHashMap, LinkedBlockingDeque, ArrayBlockingQueue, DelayQueue
    // Wrapper :  Collections.unmodifiableCollection()
    // Convenience : Collections.emptyList(); Collections.emptySet(); Collections.emptyMap(); Arrays.asList();
    // Abstract : AbstractCollection, AbstractSet, AbstractList, AbstractMap

    public static void main(String[] args) {
//        Collection -> Set, List, Queue
//        Map -> SortedMap...

    }
}

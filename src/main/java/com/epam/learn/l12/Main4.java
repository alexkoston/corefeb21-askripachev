package com.epam.learn.l12;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Main4 {
    public static void main(String[] args) {
        List<String> list = new LinkedList<>();
        list.add("barsik");
        list.add("barsik");
        list.add("mursik");
        list.add("mursik");

        // ИСПОЛЬЗОВАТЬ ДЛЯ УДАЛЕНИЯ ТЕКУЩЕГО ЭЛЕМЕНТА НЕЛЬЗЯ
        //ConcurrentModificationException
//        for (String name: list) {
//            if(name.equals("barsik")){
//                list.remove(name);
//            }
//        }

        Iterator<String> iterator = list.iterator();

        // не надо так (большая вероятность человеческой ошибки)
//        for (int i = 0; i < list.size() ; i++) {
//            if(list.get(i).equals("barsik")){
//                list.remove("barsik");
//            }
//        }

        list.removeIf(O -> O.equals("barsik"));
//        while(iterator.hasNext()){
//            String el = iterator.next();
//            if(el.equals("barsik"))
//                iterator.remove();
//        }
        System.out.println(list);
    }
}

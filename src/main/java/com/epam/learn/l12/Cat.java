package com.epam.learn.l12;

import java.sql.SQLOutput;
import java.util.Objects;

public class Cat {
    String name;
    public void setName(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        Cat barsik = new Cat();
        barsik.setName("Barsik");

        Cat barsik2 = new Cat();
        barsik2.setName("Barsik");

        System.out.println(barsik.equals(barsik2));

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cat cat = (Cat) o;
        return Objects.equals(name, cat.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}

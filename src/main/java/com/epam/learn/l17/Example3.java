package com.epam.learn.l17;

import java.util.Scanner;

public class Example3 {
    public static void main(String[] args) {
        String instr = "Name: Joe Age: 28 ID: 77";
        Scanner scanner = new Scanner(instr);
        scanner.findInLine("Age");
        if(scanner.hasNext()) {
            System.out.println(scanner.next());
        } else {
            System.out.println("Error");
        }
        scanner.close();
    }
}

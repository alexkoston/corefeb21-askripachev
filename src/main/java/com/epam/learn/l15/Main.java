package com.epam.learn.l15;

public class Main {
    private final Object lock = new Object();

    // mutually exclusive lock
    // mutex
    // принцип туалета
    //  synchronized
    // wait
    // notify
    // notifyAll

    // блокировки
    // Deadlock - посмотреть как реализовать в коде
    // Livelock *

    public static void main(String[] args) {

    }

    void doSomething(){
        System.out.println("I am here ...");

        synchronized (lock) {

        }
    }
}

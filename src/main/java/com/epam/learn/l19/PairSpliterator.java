package com.epam.learn.l19;

import java.util.Spliterator;
import java.util.function.IntConsumer;

public class PairSpliterator implements Spliterator.OfInt {
    Spliterator source;
    boolean hasPast;
    boolean hasPrevious;
    private int current;
    @Override
    public OfInt trySplit() {
        Spliterator prefixS = source.trySplit();
        if(prefixS == null) {
            return null;
        }
        return null;
    }

    @Override
    public long estimateSize() {
        return 0;
    }

    @Override
    public int characteristics() {
        return 0;
    }

    @Override
    public boolean tryAdvance(IntConsumer action) {
        return false;
    }
}

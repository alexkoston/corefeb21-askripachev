package com.epam.learn.l19;


import java.sql.Time;
import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.GregorianCalendar;

public class Main5 {
    public static void main(String[] args) {
//        LocalDateTime lovalDateTime;
//        LocalTime localTime = LocalTime.of(10,2);
//        System.out.println(localTime.plusSeconds(14));

        ZonedDateTime zonedDateTime = ZonedDateTime.of(2020,2,2,2,
                2,2,2, ZoneId.of("UTC-2"));
//        System.out.println(zonedDateTime);

        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG);
        String format = formatter.format(ZonedDateTime.now());
        System.out.println(format);

        final Instant now = Instant.now();
        final Date date = Date.from(now);
        System.out.println(date.toInstant());
        System.out.println(now);

        ZonedDateTime now1 = ZonedDateTime.now();
        GregorianCalendar calendar = GregorianCalendar.from(now1);
        System.out.println(calendar.toZonedDateTime());
        System.out.println(now1);

        LocalDateTime now2 = LocalDateTime.now();
        Timestamp timestamp = Timestamp.valueOf(now2);
        System.out.println(now2);
        System.out.println(timestamp.toLocalDateTime());

        LocalTime now3 = LocalTime.now();
        Time time = Time.valueOf(now3);
        System.out.println(now3);
        System.out.println(time.toLocalTime());
    }
}

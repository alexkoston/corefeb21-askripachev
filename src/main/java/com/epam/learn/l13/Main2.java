package com.epam.learn.l13;

import java.util.*;

public class Main2 {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("barsik", "pushokk", "barsik","murzik", "snegok", "rijik", "barsik"
                ,"murzik", "snegok");
        List<String> list2 = Arrays.asList("barsik","murzik", "snegok");

        System.out.println(Collections.indexOfSubList(list, list2));
        System.out.println(Collections.lastIndexOfSubList(list, list2));

        Collections.swap(list2, 0, 2);
        System.out.println(list2);

//        Collection<String> collection = Collections.unmodifiableCollection(list2);
//        //collection.add("afsdf");
//        //collection.remove("barsik");
//        System.out.println(collection);

        Collection<String> collection = Collections.synchronizedCollection(list2);

        Collection  collection1 = Collections.checkedCollection(list, String.class);
        //collection1.add(1);
        System.out.println(collection1);

        Set<String> singletonSet =  Collections.singleton("barsik");
        System.out.println(singletonSet);
        //singletonSet.add("asd");

        Collection<String> collection2 = Collections.nCopies(4, "barsik");
        System.out.println(collection2);

        System.out.println(Collections.frequency(collection1, "barsik"));

        //Comparator<String> comparator = Collections.reverseOrder();

        Integer[] array = new Integer[]{2, 4, 1, 3};
        Arrays.sort(array, Collections.reverseOrder());
        System.out.println(Arrays.asList(array));

        Vector<String> vector = new Vector<>();
        vector.add("barsik");
        vector.add("murzik");
        List<String> list1 = new ArrayList<>();
        list1.addAll(vector);
        System.out.println(list1);

        boolean isUnique = Collections.disjoint(list1, list2);
        System.out.println(isUnique);

        List<Integer> numList = new ArrayList<>();
        Collections.addAll(numList, array);
        System.out.println(numList);

        Map<String, Boolean> map = new HashMap<>();
        //map.put("barsik", true);
        Collections.newSetFromMap(map);

        Deque<String> deque = new LinkedList<>();
        Queue<String> queue = Collections.asLifoQueue(deque);
    }
}

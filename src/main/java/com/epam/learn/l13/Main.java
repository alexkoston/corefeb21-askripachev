package com.epam.learn.l13;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(3);
        list.add(2);
        list.add(4);

        // merge sort O(N*logN)
        Collections.sort(list);
        System.out.println(list);

        // binary search O(logN)
        System.out.println(Collections.binarySearch(list, 3));

        Collections.reverse(list);
        System.out.println(list);
        Collections.shuffle(list);
        System.out.println(list);

        List<Integer> list2 = new ArrayList<>(30);
        list2.add(1);
        list2.add(1);
        list2.add(1);
        list2.add(1);
        list2.add(1);
        System.out.println(list2);
        Collections.fill(list2,3);
        System.out.println(list2);

        List<String> list3 = Arrays.asList("barsik", "murzik", "snegok");
        // O(logN)
        System.out.println("Index of murzik is: " + Collections.binarySearch(list3, "murzik"));
        // O(N)
        System.out.println(list3.indexOf("murzik"));
        //Collections.fill(list3,"barsik");
        System.out.println(list3);

        Collection<String> collection = Arrays.asList("red", "green", "blue");
        System.out.println(Collections.max(collection));
        System.out.println(Collections.min(collection));

//        Collections.copy(list, list2);
//        System.out.println(list);
        list.addAll(list2);
        System.out.println(list);

        Collections.rotate(list3, 2);
        System.out.println(list3);

        Collections.replaceAll(list3, "murzik", "sharik");
        System.out.println(list3);
    }
}

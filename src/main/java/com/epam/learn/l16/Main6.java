package com.epam.learn.l16;

import java.io.*;

public class Main6 {
    public static void main(String[] args) throws IOException {
        PipedOutputStream pipedOutputStream;
        PipedInputStream pipedInputStream;

        int countRead = 0;
        int[] toread = null;

        pipedInputStream = new PipedInputStream();
        pipedOutputStream = new PipedOutputStream(pipedInputStream);

        for (int i = 0; i < 20; i++) {
            pipedOutputStream.write(i);
        }

        int willRead = pipedInputStream.available();
        toread = new int[willRead];

        for (int i = 0; i < willRead; i++) {
            toread[i] = pipedInputStream.read();
            System.out.println(toread[i] + " ");
        }

        FileInputStream inFile2;
        FileInputStream inFile3;

        SequenceInputStream sequenceInputStream;
        FileOutputStream outFIle;

        // I am file 2.
        inFile2 = new FileInputStream("file 2.txt");
        // I am file 3.
        inFile3 = new FileInputStream("file 3.txt");
        sequenceInputStream = new SequenceInputStream(inFile2, inFile3);
        // I am file 2.I am file 3.
        outFIle = new FileOutputStream("file 4.txt");
        int readByte = sequenceInputStream.read();
        while (readByte != -1) {
            outFIle.write(readByte);
            readByte = sequenceInputStream.read();
        }
    }
}

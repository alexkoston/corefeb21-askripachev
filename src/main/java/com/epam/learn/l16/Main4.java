package com.epam.learn.l16;

import java.io.*;

public class Main4 {
    public static void main(String[] args) {
        // InputStream |

        // абстрактный класс , описывающийпоточный ввод
        InputStream inputStream;
        // абстрактный класс , описывающийпоточный вывод
        OutputStream outputStream;

        // служит для буферизации ввода
        BufferedInputStream bufferedInputStream;
        // служит для буферизации вывода
        BufferedOutputStream bufferedOutputStream;

       // это поток читающий из массива байт
        ByteArrayInputStream byteArrayInputStream;
        // это поток пишущий в массив байт
        ByteArrayOutputStream byteArrayOutputStream;

        // поток ввода, который содержит методы для чтения данных стандартных типов java
        DataInputStream dataInputStream;
        // содержит методы для записи стандартных типов java
        DataOutputStream dataOutputStream;

        // читает байты из файла
        FileInputStream fileInputStream;
        // пишет байты в файл
        FileOutputStream fileOutputStream;

        // adapter pattern
        FilterInputStream fileInputStream1;
        FilterInputStream filterInputStream;

        // поток для сериализации объектов
        ObjectInputStream objectInputStream;
        // для десериализации объектов
        ObjectOutputStream objectOutputStream;

        // Поток читающий данные из канала
        PipedInputStream pipedInputStream;
        // Поток пишущий данные в канал
        PipedOutputStream pipedOutputStream;

        // используется для конвертации и записи строк в байтовый поток
        PrintStream printStream;
        // Позволяет вернуть в поток считанные из него данные
        PushbackInputStream pushbackInputStream;
        // считывает данные из других двух и более входных потоков
        SequenceInputStream sequenceInputStream;

        // read

    }
}

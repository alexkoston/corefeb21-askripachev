package com.epam.learn.l16;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {
    // stream
    // in / out

    // source
    // destination

    // text-streams (символьные потоки - последовательность 16-битовых символов  Unicode)
    // binary-streams (байтовые потоки)

    // open for read/write close!
    // create object
    public static void main(String[] args) {
        // before java 7
//        PrintWriter pw = null;
//        try {
//            // create stream obj | open stream
//            FileWriter out = new FileWriter("text.txt");
//
//            // set settings
//            BufferedWriter br = new BufferedWriter(out);
//            pw = new PrintWriter(br);
//            pw.println("I am a sentence in a text file.");
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (pw != null) {
//                pw.close();
//            }
//        }

        // since java 7
        try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter("text2.txt")))) {
            pw.println("Hello, I am from java 7");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

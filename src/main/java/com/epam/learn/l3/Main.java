package com.epam.learn.l3;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Objects;

public class Main {
    private static boolean isTrue = true;
    public static void main(String[] args) {
        // reader - позволяет считывать с консоли
//        String line = null;
//        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))){
//            //reader.readLine();
//            line = reader.readLine();
//        }catch(IOException e){
//            e.printStackTrace();
//        }

//        int finalResult = Integer.parseInt(Objects.requireNonNull(line));

        // в такой конструкции если приходит результат отличный от 3 - у нас всегда красный
//        if(finalResult == 1){
//            System.out.println("green");
//        } else if (finalResult == 2){
//            System.out.println("yellow");
//        } else
//            System.out.println("red");
        Colours colours = Colours.RED;
        if (getColorByTime(colours) == Colours.GREEN){
            System.out.println("GREEN");
        } else if (getColorByTime(colours) == Colours.YELLOW){
            System.out.println("YELLOW");
        } else if (getColorByTime(colours) == Colours.RED){
            System.out.println("RED");
        }
    }
    private static Colours getColorByTime(Colours colour){
        return colour;
    }
}

package com.epam.learn.l1;

public class PrimitiveExample {
    // целые
    byte someByte;      // -128 127         8 bit
    short someShort;    // -2^15 2^15 - 1   16 bit
    int number = 1;     // -2^31 2^31 - 1   32 bit
    long bigNumber;     // -2^63 2^63 - 1   64 bit

    // дробные
    float nFloat;       //                  32 bit
    double nDouble;     //                  64 bit

    // символьный тип
    char someChar;      //                  16 bit

    // логический тип
    boolean isTrue;
}

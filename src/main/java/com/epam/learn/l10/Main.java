package com.epam.learn.l10;

import java.util.*;

public class Main {
    // array - []
    public static void main(String[] args) {
        // минусы массивов - фикс. длина
        // имеют ограниченные методы обработки

        int[]  arr = new int[2];
        arr[1] = 20;


        // legacy - Legacy Collections Java 1.0 // 1.2
        Enumeration enumeration;
        Vector vector;
        Stack stack;

        // Map
        Properties properties;
        Hashtable hashtable;

        BitSet bitSet;
    }
}

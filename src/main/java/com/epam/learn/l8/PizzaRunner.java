package com.epam.learn.l8;

public class PizzaRunner {
    private PizzaInterface pizzaMsk;
    private PizzaInterface pizza;
    private PizzaInterface pizzaSpb;

    public static void main(String[] args) {
        PizzaRunner runner = new PizzaRunner();
        runner.getPizzaInfo();
    }

    public void getPizzaInfo(){
        if(true){
            pizza = new PizzaMSK();
        } else{
            pizza = new PizzaSpb();
        }
        pizzaMsk = new PizzaMSK();
        pizzaSpb = new PizzaSpb();
        //pizzaSpb.sayHello();
        pizzaMsk.wash();
        pizzaSpb.wash();
        pizzaMsk.cook();
        pizzaSpb.cook();
        pizzaMsk.delivery();
        pizzaSpb.delivery();
    }
}

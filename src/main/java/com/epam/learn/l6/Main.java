package com.epam.learn.l6;

public class Main {


    public static void main(String[] args) {
        StringBuilder sb;
        StringBuffer sbf;

        String name = "Va" + "Si" + "Li" + "Sk";

        sb = new StringBuilder("barsik");
        System.out.println(sb.length());
        System.out.println(sb.charAt(2));

        sb.setCharAt(2, 'R');

        System.out.println(sb.subSequence(2,4));

        sb.append(" the").append(" cat");

        sb.insert(10, " good");
        System.out.println(sb);

        sb.reverse();
        System.out.println(sb);



    }

    private String getInfo(){
        return "ba" + "at";
    }
}

package com.epam.learn.l2;

public class Logic {
    private boolean paramA = true;
    private boolean paramB = false;
    private boolean paramC= true;

    public static void main(String[] args) {
        Logic logic = new Logic();
        System.out.println(logic.checkResultFirst(logic.paramA, logic.paramB,logic.paramC));
        System.out.println(logic.checkResultSecond(logic.paramA,logic.paramB));
        System.out.println(logic.checkResultThird(logic.paramB,logic.paramC));
        System.out.println(logic.checkResultFirst(logic.paramA, logic.paramB,logic.paramC));

    }
    // A && B || C
    private boolean checkResultFirst (boolean a , boolean b, boolean c){
        return a && b || c;

    }
    private boolean checkResultSecond (boolean a , boolean b){
        return a && b;

    }
    private boolean checkResultThird (boolean b, boolean c){
        return c || b;

    }
    private boolean checkResultFourth (boolean a , boolean b, boolean c){
        return a || b && c;

    }
}
